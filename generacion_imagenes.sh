#!/bin/bash

#Creacion de red docker para no depender de la ip del anfitrion
docker network create --subnet=172.18.0.0/16 red_docker


#Clonacion de repositorio de archivos para el backend
git clone https://gitlab.com/larryss18/backend_suma.git
#Clonacion de repositorio de archivos para  nginx
git clone https://gitlab.com/larryss18/backend_proxy.git
#Clonacion de repositorio de archivos para postgres
git clone https://gitlab.com/larryss18/backend_database.git

#Generacion de imagenes 

origen_dir=$(pwd)

cd backend_suma
docker build -t backend/suma .
cd $origen_dir

cd backend_proxy
docker build -t proxy/suma .
cd $origen_dir

cd backend_database 
docker build -t database/suma .




